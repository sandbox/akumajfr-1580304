<?php

/**
 * @file
 * Administration pages provided by Webform External module.
 */

/**
 * Menu callback for node/%webform_menu/webform/external.
 */
function webform_external_admin_settings() {
  if (user_access('specify external datasource')) {
    global $databases;

    $datasources = array();

    foreach ($databases as $key => $value) {
      $datasources[$key] = t($key);
    }

    if (arg(0) == 'node') {
      $nid = arg(1);
    }

    $query = db_select('webform_external_datasource', 'wed');

    $query
    ->condition('wed.nid', $nid, '=')
    ->fields('wed', array('datasource'));

    $result = $query->execute()->fetch();

    if (!$result) {
      $default_value = "";
    }
    else {
      $default_value = $result->datasource;
    }

    $form['datasource']  = array(
      '#type' => 'select',
      '#title' => t('Data Source'),
      '#description' => t('The data source where submissions for this form should be stored.'),
      '#default_value' => $default_value,
      '#options' => $datasources,
    );

    $form['#submit'][] = 'webform_external_admin_settings_submit';
    $form = system_settings_form($form);

    return $form;
  }
}

/**
 * Submit handler for webform_external_admin_settings().
 */
function webform_external_admin_settings_submit($form, &$form_state) {
  $datasource = $form_state["values"]["datasource"];

  if (arg(0) == 'node') {
    $nid = arg(1);
  }

  $merge_query = db_merge('webform_external_datasource')
  ->key(array('nid' => $nid))
  ->fields(array(
    'nid' => $nid,
    'datasource' => $datasource,
  ))
  ->execute();

  db_set_active($datasource);

  if (!db_table_exists('webform_submitted_data')) {

    $create_table_sql = "CREATE TABLE `webform_submitted_data` (
 		`nid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The node identifier of a webform.',
 		`sid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The unique identifier for this submission.',
 		`cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT 'The identifier for this component within this node, starts at 0 for each node.',
 		`no` varchar(128) NOT NULL DEFAULT '0' COMMENT 'Usually this value is 0, but if a field has multiple values (such as a time or date), it may require multiple rows in the database.',
 		`data` mediumtext NOT NULL COMMENT 'The submitted value of this field, may be serialized for some components.',
 		PRIMARY KEY (`nid`,`sid`,`cid`,`no`),
 		KEY `nid` (`nid`),
 		KEY `sid_nid` (`sid`,`nid`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all submitted field data for webform submissions.'";

    $result = db_query($create_table_sql)->execute();
  }

  db_set_active();
}
