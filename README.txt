-- SUMMARY --

The Webform External module allows users of webform to store the data for 
specific webforms in an external datasource that is defined in the settings.php 
file. This was created in response to forms that contained data protected by 
HIPAA regulations, which requires that HIPAA-defined data be stored separately 
from other data and be closely monitored.

Keep in mind that this module provides no provisions for backup of external 
data.

-- REQUIREMENTS --

Webform 3.x

-- INSTALLATION --

* Install as usual, 
see http://drupal.org/documentation/install/modules-themes/modules-7 for more 
information.

-- CONFIGURATION --

Adds an additional menu item when creating new or editing existing Webform 
nodes called External DB. Select desired datasource to store webform data 
from a dropdown list. List is generated from available datasources in the 
settings.php file.

Database configuration should be created as such:

$databases = array (
  'default' => 
  array (
    'default' => 
    array (
      'database' => 'main_datasource',
      'username' => 'drupal',
      'password' => '',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
  'external_datasource' =>
  array(
    'default' =>
    array (
      'database' => 'external_datasource',
      'username' => 'drupal',
      'password' => '',
      'host' => 'localhost',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

When an external datasource is specified and the config is saved, the module 
checks that datasource for the webform_submitted_data table. If the table does 
not exist, the module will create it for you.
